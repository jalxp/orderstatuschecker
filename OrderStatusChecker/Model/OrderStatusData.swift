//
//  OrderStatusData.swift
//  OrderStatusChecker
//
//  Created by João Pereira on 18/03/2021.
//

import UIKit

struct OrderStatusData: Codable {
    let status: String
    let shipping_tracking_num: String
    let shipping_tracking_url: String
    
    var color: UIColor {
        var color: UIColor
        switch status {
        case "PENDING":
            color = .systemYellow
        case "FULFILLED":
            color = .systemOrange
        default:
            color = .systemGreen
        }
        return color
    }
}
