//
//  OrderStatusManager.swift
//  OrderStatusChecker
//
//  Created by João Pereira on 18/03/2021.
//

import Foundation

protocol OrderStatusManagerDelegate {
    func didUpdateOrderStatus(_ orderStatusManager: OrderStatusManager, orderStatus: OrderStatusData)
    func didFailWithError(error: Error)
}

struct OrderStatusManager {
    
    var delegate: OrderStatusManagerDelegate?
    
    private let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MjcwMjE1NywidGltZXN0YW1wIjoiMjAyMS0wMy0xNyAxNDoyNjowNy4"
    private let baseURL = "https://commerce.goodbarber.dev/publicapi/v1/general/orders/%@/order/%@/shipping/"
    private let webzineID = "2702157"
    
    func getOrderStatus(for orderID: String) {
        let url = String(format: baseURL, webzineID, orderID)
        performRequest(with: url)
    }
    
    private func performRequest(with urlString: String) {
        if let url = URL(string: urlString) {
            var request = URLRequest(url: url)
            // Set header
            request.setValue(token, forHTTPHeaderField: "token")
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: request) { (data, response, error) in
                if error != nil {
                    delegate?.didFailWithError(error: error!)
                }
                if let safeData = data {
                    if let orderStatusData = parseJSON(safeData) {
                        delegate?.didUpdateOrderStatus(self, orderStatus: orderStatusData)
                    }
                }
            }
            task.resume()
        }
    }
    
    private func parseJSON(_ data: Data) -> OrderStatusData? {
        let decoder = JSONDecoder()
        do {
            let decodedData = try decoder.decode(OrderStatusData.self, from: data)
            return OrderStatusData(status: decodedData.status, shipping_tracking_num: decodedData.shipping_tracking_num, shipping_tracking_url: decodedData.shipping_tracking_url)
        } catch {
            delegate?.didFailWithError(error: error)
            return nil
        }
    }
}
