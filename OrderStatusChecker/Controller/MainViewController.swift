//
//  ViewController.swift
//  OrderStatusChecker
//
//  Created by João Pereira on 18/03/2021.
//

import UIKit

class MainViewController: UIViewController {
    
    //MARK: Views and Subviews
    private let mainContentView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 20
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    } ()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Order Status Checker"
        label.textAlignment = .center
        label.textColor = .systemBlue
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    } ()
    
    private let orderIDTextField: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = .white
        textField.placeholder = "Your order ID"
        textField.borderStyle = .roundedRect
        textField.keyboardType = .decimalPad
        textField.clearButtonMode = .always
        textField.textAlignment = .center
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    } ()
    
    private let checkOrderButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .systemBlue
        button.setTitle("Check Order", for: .normal)
        button.tintColor = .white
        button.layer.cornerRadius = 10
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(checkOrderPressed), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    } ()
    
    @objc func checkOrderPressed(_ sender: UIButton!) {
        if let orderID = orderIDTextField.text {
            if !orderID.isEmpty {
                let orderStatusVC = OrderStatusViewController()
                orderStatusVC.orderID = orderID
                self.present(orderStatusVC, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: setupAutoLayout
    func setupAutoLayout() {
        let safeArea = view.safeAreaLayoutGuide
        
        mainContentView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        mainContentView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        mainContentView.topAnchor.constraint(equalTo: safeArea.topAnchor).isActive = true
        mainContentView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor).isActive = true
        
        titleLabel.topAnchor.constraint(equalTo: mainContentView.topAnchor, constant: 10).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: mainContentView.leftAnchor, constant: 40).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: mainContentView.rightAnchor, constant: -40).isActive = true
        
        orderIDTextField.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10).isActive = true
        orderIDTextField.leftAnchor.constraint(equalTo: mainContentView.leftAnchor, constant: 40).isActive = true
        orderIDTextField.rightAnchor.constraint(equalTo: mainContentView.rightAnchor, constant: -40).isActive = true
        orderIDTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        checkOrderButton.topAnchor.constraint(equalTo: orderIDTextField.bottomAnchor, constant: 20).isActive = true
        checkOrderButton.leftAnchor.constraint(equalTo: mainContentView.leftAnchor, constant: 40).isActive = true
        checkOrderButton.rightAnchor.constraint(equalTo: mainContentView.rightAnchor, constant: -40).isActive = true
        checkOrderButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    //MARK: viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainContentView.addSubview(titleLabel)
        mainContentView.addSubview(orderIDTextField)
        mainContentView.addSubview(checkOrderButton)
        
        view.backgroundColor = .white
        view.addSubview(mainContentView)
        
        setupAutoLayout()
        
        #warning("Remove me")
        orderIDTextField.text = "280197"
        
    }
}
