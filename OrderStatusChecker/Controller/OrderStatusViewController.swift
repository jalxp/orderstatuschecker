//
//  OrderStatusViewController.swift
//  OrderStatusChecker
//
//  Created by João Pereira on 18/03/2021.
//

import UIKit

class OrderStatusViewController: UIViewController {
    var orderStatusManager = OrderStatusManager()
    var orderID: String = ""
    
    //MARK: Views and Subviews
    private let statusContentView: UIView = {
        let view = UIView()
        view.backgroundColor = .systemTeal
        view.layer.cornerRadius = 20
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    } ()
    
    private let orderStatusTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Status"
        label.textAlignment = .center
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 40)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    } ()
    
    private let orderStatusLabel: UILabel = {
        let label = UILabel()
        label.text = "..."
        label.textAlignment = .center
        label.backgroundColor = .white
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 25)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    } ()
    
    let backButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .systemBlue
        button.setTitle("Back", for: .normal)
        button.tintColor = .white
        button.layer.cornerRadius = 10
        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(backPressed(_:)), for: .touchUpInside)
        
        return button
    } ()
    
    @objc func backPressed(_ sender: UIButton!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: AutoLayout
    func setupAutoLayout() {
        statusContentView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        statusContentView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        statusContentView.heightAnchor.constraint(equalToConstant: view.frame.height).isActive = true
        statusContentView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        orderStatusTitleLabel.topAnchor.constraint(equalTo: statusContentView.topAnchor, constant: 40).isActive = true
        orderStatusTitleLabel.leftAnchor.constraint(equalTo: statusContentView.leftAnchor, constant: 40).isActive = true
        orderStatusTitleLabel.rightAnchor.constraint(equalTo: statusContentView.rightAnchor, constant: -40).isActive = true
        orderStatusTitleLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        orderStatusLabel.topAnchor.constraint(equalTo: orderStatusTitleLabel.bottomAnchor, constant: 10).isActive = true
        orderStatusLabel.leftAnchor.constraint(equalTo: orderStatusTitleLabel.leftAnchor, constant: 40).isActive = true
        orderStatusLabel.rightAnchor.constraint(equalTo: orderStatusTitleLabel.rightAnchor, constant: -40).isActive = true
        orderStatusLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        backButton.topAnchor.constraint(equalTo: orderStatusLabel.bottomAnchor, constant: 20).isActive = true
        backButton.leftAnchor.constraint(equalTo: orderStatusLabel.leftAnchor, constant: 40).isActive = true
        backButton.rightAnchor.constraint(equalTo: orderStatusLabel.rightAnchor, constant: -40).isActive = true
        backButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
    }
    
    // MARK: viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        orderStatusManager.delegate = self
        orderStatusManager.getOrderStatus(for: orderID)
        
        statusContentView.addSubview(orderStatusTitleLabel)
        statusContentView.addSubview(orderStatusLabel)
        statusContentView.addSubview(backButton)
        
        view.addSubview(statusContentView)
        
        setupAutoLayout()
    }
}

//MARK: - OrderStatusManagerDelegate
extension OrderStatusViewController: OrderStatusManagerDelegate {
    func didUpdateOrderStatus(_ orderStatusManager: OrderStatusManager, orderStatus: OrderStatusData) {
        DispatchQueue.main.async {
            self.orderStatusLabel.text = orderStatus.status
            self.orderStatusLabel.textColor = orderStatus.color
        }
    }
    
    func didFailWithError(error: Error) {
        print(error)
    }
    
}
